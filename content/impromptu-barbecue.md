---
title: "Impromptu barbecue"
author: Jan Hettenkofer
createdAt: 2020-05-04
updatedAt: 2020-05-04
image: /img/impromptu-barbecue/0405_barbecue_CF004542.jpg
tags:
    - food
    - photography
---


This is an impromptu portrait of a barbecue session. No preparation, no lighting, only a hungry bunch of people telling me to hurry up.

<!--more-->

<bz-picture loading="lazy" alt="Barbecue ingredients: marinated pork, bell peppers, feta cheese, eggs as well as a shaker of chili flakes" src="/img/impromptu-barbecue/0405_barbecue_CF004533.jpg"></bz-picture>

<bz-picture loading="lazy" alt="Egg-and-cheese bell peppers, pork chops, sausage and cheese on the grill" src="/img/impromptu-barbecue/0405_barbecue_CF004542.jpg"></bz-picture>

<bz-picture loading="lazy" alt="A pint of German wheat beer, or: eine halbe Weiße; the glass displays the brewery's tower, designed by Friedensreich Hundertwasser" src="/img/impromptu-barbecue/0405_barbecue_CF004548.jpg"></bz-picture>
