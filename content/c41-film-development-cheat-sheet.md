---
title: "C41 Film Development Cheat Sheet (CineStill CS41)"
author: Jan Hettenkofer
image: /img/rolleiflex-2-8-e-field-report/rolleiflex_portrait.jpg
createdAt: 2022-07-02
updatedAt: 2022-07-02
tags:
    - analogue-photography
    - photography
---

I don't trust myself not to make a mistake in adjusting C41 development times for developer exhaustion or push while also keeping track of temperature, agitation and all the other times. So I made a neat chart to look those times up and you can have it!

<!--more-->

Here's a preview. If you like it, click the button below to get the PDF.

<a href="/cs41_cheat_sheet.pdf" download target="_blank"><bz-picture loading="lazy" alt="A chart with development times for CineStill CS41 colour film development chemistry" src="/img/c41_cheat_sheet/cs41_cheat_sheet.png"></bz-picture></a>

<div class="text-center">
    <a href="/cs41_cheat_sheet.pdf" download target="_blank" class="cta-link">Download the CS41 cheat sheet!</a>
</div>

This chart is specific to CineStill CS41 chemistry. If you want to adapt it to another manufacturer's specifications (or if you have any feedback at all), [message me on Twitter](https://twitter.com/JanHettenkofer).
