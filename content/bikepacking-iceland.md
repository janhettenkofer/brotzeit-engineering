---
title: "Bikepacking Iceland in October - Reykjavik to Snæfellsnes"
author: Jan Hettenkofer
image: /img/bikepacking-iceland/twig_vatnaleidh.jpg
createdAt: 2023-11-22
updatedAt: 2023-11-22
tags:
    - travel
    - photography
---

Iceland is quiet. If you were to pick any spot at random on this landmass known for its thundering waterfalls, growling volcanic activity and bustling culture, chances are, you would hear nothing at all when you go there. However, this silence foreshadows the might that nature bears on this island. Such was my experience when I pedaled a good 530 km from Reykjavik to Reykholt, from Þingvellir to Hvalfjörður and from Varmaland to Grundarfjörður.

<!--more-->

> Some parts of this article are yet to be written

## Reykjavik: Expectations, tempered

Iceland is _the_ bucket-list destination for many people. Travellers returning from the land of ice and fire tell tales of life-changing experiences. Of true wilderness. Of the midnight sun and the northern lights.

If you go with the flow - no, the flood - of visitors getting swept from Keflavik airport to the capital multiple times per day, these expectations are only intensified by the all-too-prominent marketing of a well-oiled tourism industry. More than once, I felt like Reykjavik was set up as one enormous gift-shop for this adventure park the vikings unwittingly set up between tectonic plates. This was not what I had come to see.

<full-bleed-section>
<div class="grid grid-cols-1 md:grid-cols-2 gap-4 w-full px-4 relative">
    <bz-picture rounded loading="lazy" alt="The Harpa concert hall" src="/img/bikepacking-iceland/iceland_2023_DSC_7814.jpg"></bz-picture>
    <div class="grid grid-cols-1 gap-4">
        <figure>
            <bz-picture rounded loading="lazy" alt="My steel bike, packed with frame-, fork-, seat- and handlebar bags in front of the colourful façade of Brauð & Co's city-centre branch" src="/img/bikepacking-iceland/iceland_2023_DSC_7822.jpg"></bz-picture>
            <figcaption>Hidden behind my bike is Brauð & Co, a bakery dealing in the most delicious pastries I have ever had.</figcaption>
        </figure>
        <bz-picture rounded loading="lazy" alt="Hallgrimskirkja framed by the buildings of central Reykjavik" src="/img/bikepacking-iceland/iceland_2023_DSC_7832.jpg"></bz-picture>
    </div>
</div>
</full-bleed-section>

My hopes and dreams for Iceland were built on recipes for odd foods I had seen in Magnus Nilsson's bibles on Nordic [Cooking](https://www.phaidon.com/store/cookbooks-food-and-drink/the-nordic-cookbook-9780714868721/) and [Baking](https://www.phaidon.com/store/cookbooks-food-and-drink/the-nordic-baking-book-9780714876849/). On the intrigue of tomatoes growing in a cold country that is deprived of sunlight half of the year. On the delightful - and incorrect - statistic of Iceland having the highest number of Nobel Prize winners per capita, despite having only one Nobel Prize winner. And of course, on the promise of the finest gravel roads in the world.

Walking away from the make-believe façades of souvenir shops, I started my journey of reconciling these hopes and dreams with reality. After five days in Reykjavik, my confusion started to give way to a tiny bit of understanding.

<div class="grid grid-cols-1 md:grid-cols-2 gap-4 w-full relative">
    <div>

### Appearances

- While Icelandic façades often crumble, what is contained therein is kept impeccably neat
- The hospitality industry is staffed primarily by expats and accordingly going from one location to the next sometimes feels like travelling to their respective home countries
- Knitting is the coolest thing to do
    </div>
    <div>

### Food

- French-style pâtisserie as well as specialty coffee are taken extraordinarily seriously
- Filter coffee is actually good in Iceland and you often get free refills, making it the only cheap-ish thing I found
- Supermarkets have excellent canned products, decent-but-expensive vegetables and dreadful meat
- Icelandic craft beer is lovely
    </div>
    <div>

### Practicalities

- 1000 ISK are roughly equivalent to 7€ and I'm now really good at multiplying by seven
- Gas cartridges for camping stoves can be had for free at many accomodations near the airport - people buy way more than they need and leave what's left for future travellers
- Icelanders have a wonderfully dry sense of humour
    </div>
    <div>

### Sightseeing

- It's not about what you see in front of your eyes, it's what you feel there
- The best spots are not on the map - in fact, they are often in-between spots
- The cutest bookshop is called Salka
    </div>
</div>

<full-bleed-section>
<div class="grid grid-cols-1 md:grid-cols-2 gap-4 w-full px-4 relative">
    <div class="grid grid-cols-1 gap-4">
        <figure>
            <bz-picture rounded loading="lazy" src="/img/bikepacking-iceland/iceland_2023_DSC_7836.jpg"></bz-picture>
            <figcaption>The Recycled House, a curious pile of rusting steel, decoratively welded into a home.</figcaption>
        </figure>
        <figure>
            <bz-picture rounded loading="lazy" alt="The sunset over Reykjavik's skyline" src="/img/bikepacking-iceland/iceland_2023_DSC_7882.jpg"></bz-picture>
            <figcaption>Reykjavik is most scenic when seen against the setting sun.</figcaption>
        </figure>
    </div>
    <bz-picture rounded loading="lazy" alt="The sunset over Reykjavik's skyline" src="/img/bikepacking-iceland/iceland_2023_DSC_7858.jpg"></bz-picture>
</div>
</full-bleed-section>

Two days before I headed out of Reykjavik, I spoke to a tour guide I had met in a craft beer pub. He recommended that I avoid the main road towards Þingvellir, and instead take the road number 435 towards the southern tip of Þingvallavatn. Turning to the barkeeper, he added "Siggi, if this guy comes back alive in two weeks, give hime a free beer".

> **Important:** The base maps used in this article are sourced from [LMÍ](https://gatt.lmi.is/geonetwork/srv/eng/catalog.search#/metadata/976bafc8-681a-40b2-a65d-62a412e8f786) and are licensed according to LMÍ's terms, NOT the CC-by-nc-sa license I usually use. Please refer to LMÍ for license information.

<full-bleed-section><picture-background legacyFormat="jpeg" loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/map_reykjavik_to_ulfljotsvatn.jpg" class="text-black">

## Reykjavik to Úlfljótsvatn: Tires to Tarmac
</picture-background></full-bleed-section>

Fully intending to come back alive two weeks later, I heeded his advice and took the smaller road. Turns out this road runs parallel to a pipeline supplying geothermally heated water to Reykjavik. Foolishly, I put off taking a photo of this pipeline until it was well out of sight. I also put off taking a photo of the scenic geothermal area it originated from - this time because the relatively flat road had turned into a string of steeply climbing and falling hills.

Only when I arrived at Úlfljótsvatn, the small lake just south of Þingvallavatn, had I recovered enough to snap a few photos of this gorgeous treeline in the sunset.

<full-bleed-section><div class="grid grid-cols-1 md:grid-cols-2 gap-4 w-full px-4 relative">
    <bz-picture rounded loading="lazy" alt="A car driving down a very well-kept road by the lakeside" src="/img/bikepacking-iceland/iceland_2023_DSC_7906.jpg"></bz-picture>
    <bz-picture rounded loading="lazy" alt="A line of reddening as well as evergreen trees reflecting in the lake during sunset" src="/img/bikepacking-iceland/iceland_2023_DSC_7898.jpg"></bz-picture>
</div></full-bleed-section>

Somewhat impressed with myself and still anxious about how cold the night would really be, I went to sleep.

<full-bleed-section><bz-picture loading="lazy" alt="Dawn over Úlfljótsvatn" src="/img/bikepacking-iceland/iceland_2023_DSC_7914.jpg"></bz-picture></full-bleed-section>

As if to encourage me for the way ahead, the dawn that greeted me the next morning painted the skies pink. Reminding me that I was still plenty green behind the ears was the cold that my sleeping bag had successfully kept out during the night.

<picture-background legacyFormat="jpeg" rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/map_ulfljotsvatn_to_reykholt.jpg" class="text-black">

## Úlfljótsvatn to Reykholt
</picture-background>

Hoping to get to the much-lauded sights of the Golden Circle, I pressed on towards Reykholt, a town halfway to Gullfoss and home to Friðheimar, a farm growing tomatoes in geothermally heated greenhouses. While the I had been relatively unbothered by cars on previous day's ride, I soon realised that I would be haunted by them for the rest of the day. What's more, the road was clearly designed to get faster vehicles than Twig, my bike, to their destination, rather than for a cyclist to enjoy the vista of a long ride.

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_7923.jpg"></bz-picture>

This steampunk contraption provided a welcome change of scenery halfway through. Otherwise, the way to the promised wonders proved quite the letdown for me.

### It's not about what you see in front of your eyes

Even though the landscape fell short of the sky-high expectations social-media coverage about Iceland had built in me, I did find treasure at the end of this day's road in the form of my fellow campers in Reykholt. We discussed bikepacking setups with a father and his daughter having a trip together to celebrate her graduation from school and a group of incredibly kind Moroccans shared photos of them cooking elaborate meals in the most incredible locations - they'd even brought a full Tajine set with them on the plane. One of them also freshly made a bread called Makhmer or Batbout and gave me some to try: utterly delicious! Even though it was just one evening, this episode somehow occupies multiple days in my memory.

<full-bleed-section><picture-background legacyFormat="jpeg" loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/map_reykholt_to_thingvellir.jpg" class="text-black">

## Reykholt to Þingvellir
</picture-background></full-bleed-section>

<full-bleed-section><div class="grid grid-cols-1 md:grid-cols-3 gap-4 w-full px-4 relative">
<figure class="md:col-span-2">
    <bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_7929.jpg"></bz-picture>
    <figcaption>Finally, gravel!</figcaption>
</figure>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_7956.jpg"></bz-picture>
</div></full-bleed-section>

<full-bleed-section><div class="grid grid-cols-1 md:grid-cols-2 gap-4 w-full px-4 relative">
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_7941.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_7950.jpg"></bz-picture>
</div></full-bleed-section>

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_7968.jpg"></bz-picture>

<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_7972.jpg"></bz-picture></full-bleed-section>
<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8036.jpg"></bz-picture></full-bleed-section>

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8056.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8065.jpg"></bz-picture>

## Exploring Þingvellir

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8069.jpg"></bz-picture>
<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8080.jpg"></bz-picture></full-bleed-section>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8099.jpg"></bz-picture>

<full-bleed-section><picture-background legacyFormat="jpeg" loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/map_thingvellir_to_bjarteyjarsandur.jpg" class="text-black">

## Þingvellir to Bjarteyjarsandur
</picture-background></full-bleed-section>

66 km. This was to be the longest ride of the entire trip. Even though I was slightly worried about the distance, I was excited about the prospect of cycling around Hvalfjörður - a bay named after the whaling industry that had - and to a degree still has - its mainstay there.

<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8179.jpg"></bz-picture></full-bleed-section>

<full-bleed-section><div class="grid grid-cols-1 md:grid-cols-2 gap-4 w-full px-4 relative">
    <bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8158.jpg"></bz-picture>
    <div class="prose">

Heading north, the landscape was becoming more and more dramatic. Along the way, I chanced upon Þórufoss, one of the many Game of Thrones shooting locations scattered across Iceland, but the promise of a fjord kept me pressing on. When the ocean came in sight, I was finally sure that I was on the right track.
    </div>
</div></full-bleed-section>

Never had I seen a paved road so perfectly layered into the landscape. The trio of dashed-solid-dashed lines seemed to pull me along and when I looked to the left, I could see the opposite shore, where I hoped I'd find not only a place to camp, but also a freshly prepared meal at a sheep farm.

<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8190.jpg"></bz-picture></full-bleed-section>

It was just after Hvammsvík, a smaller bay inside Hvalfjörður, that I realised that this seemingly short distance that separated me from my destination would taunt me for the rest of the day while the gentle slopes I had previously admired would throw cruel abuse at my legs. Every descent seemed to be followed by a climb twice as long, and the wind seemed determined to blow in my face any time I had elevation to gain.

To rack up the pressure and pile on the misery, I was soon robbed of the sun's warmth by the mountain-range on my right and the light's fading was thrown in stark relief by the ever-spreading shade these rocks threw on the opposing shore. By the time I steered my bike around the bend marking the end of the bay, I was shivering and exhausted.

<full-bleed-section><div class="grid grid-cols-1 md:grid-cols-3 gap-4 w-full px-4 relative">
    <bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8225.jpg"></bz-picture>
    <div class="grid grid-cols-1 gap-4 md:col-span-2">
        <bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8214.jpg"></bz-picture>
        <div class="prose">

There is a spot marked "Beached Whalers" on the map that I had intended to explore. However, running in automatic mode, I would have missed it if it weren't for this panorama. I parked my bike and let out a primal cry. This was my cue that I had almost made it! And boy, was it a pretty sight.

I took it all in as long as my freezing fingers would let me, snapped a few beauty shots of Twig, and hopped back on the saddle.
        </div>
    </div>
</div></full-bleed-section>

As I set off again for the last few kilometres under the watchful eyes of the particularly impressive specimen of Icelandic sheep pictured below, it occurred to me how incredibly resilient these animals were. Braving most of the year outside, seemingly unbothered by rain, wind or cold. Giving my woolly acquaintance a wave, I gave myself a pat on the back for being half as tough as they are and a nod of appreciation for all the marvellous experiences I had because of that.

<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8259.jpg"></bz-picture></full-bleed-section>

Buffeted by winds whipped up to a fresh frenzy, I made it up the last hill, down the last descent and onto the lot of Bjarteyjarsandur farm. With my knees weak as jelly, I was disappointed to find that their restaurant was only open to groups during the off-season. But more importantly, I found the wonderful company of an older American couple with excellent advice on camping sleep systems as well as a free pack of pasta and a jar of tomato sauce. Those were good.

## Hiking Glymur

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8283.jpg"></bz-picture>

After the previous days' exhaustion, I wanted to take it easy. I had passed Glymur, the highest waterfall on Iceland, and decided that a hike to the top was just the thing to do to relax.

It was perfect, in a way. Along the first few kilometres of the hike I found a smattering of peaceful, scenic spots, that were a welcome change to the hard-earned wins I had experienced before. When the path steepened, the quantised effort required to climb step after step seemed easy compared to the constant pull of gravity on my drivtrain.

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8290.jpg"></bz-picture>

<full-bleed-section><div class="grid grid-cols-1 md:grid-cols-3 gap-4 w-full px-4 relative">
    <div class="prose">

Similar to what I'd experienced on my ride to Bjarteyjarsandur, I was also regularly rewarded with glimpses of my destination.
    </div>
    <bz-picture rounded loading="lazy" class="md:col-span-2" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8299.jpg"></bz-picture>
</div></full-bleed-section>

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8311.jpg"></bz-picture>
<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8328.jpg"></bz-picture></full-bleed-section>

<full-bleed-section><div class="grid grid-cols-1 md:grid-cols-3 gap-4 w-full px-4 relative">
    <bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8368.jpg"></bz-picture>
    <bz-picture rounded loading="lazy" class="md:col-span-2" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8345.jpg"></bz-picture>
</div></full-bleed-section>

<picture-background legacyFormat="jpeg" rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/map_bjarteyjarsandur_to_fossatun.jpg" class="text-black">

## Bjarteyjarsandur to Fossatún
</picture-background>

<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8393.jpg"></bz-picture></full-bleed-section>
<full-bleed-section><div class="grid grid-cols-1 md:grid-cols-3 gap-4 w-full px-4 relative">
    <div class="prose">

When I wrote that the highlights were the in-between-places, I was thinking about places like this valley. When I rolled over the crest of the hill and caught the first glimpse of this landscape, all weariness was replaced with joy.
    </div>
    <bz-picture rounded loading="lazy" class="md:col-span-2" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8397.jpg"></bz-picture>
</div></full-bleed-section>

<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8434.jpg"></bz-picture></full-bleed-section>

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8479.jpg"></bz-picture>

<picture-background legacyFormat="jpeg" rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/map_fossatun_to_varmaland.jpg">

## Fossatún to Hotel Varmaland
</picture-background>

With no campsites on the map and -3°C on the weather report, I decided that another night in heated accomodation wouldn't be so bad. I was also in need of a resupply run and it seemed that the only option was a supermarket at a service station in Reykholt - another town by the same name as the one I'd visited earlier.

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8461.jpg"></bz-picture>

With an esepecially lovely sunrise to see me off, I took a last photo of Trollfoss and rolled towards the unimaginable luxury of a hotel room.

Harbouring some hope of potentially making a run for Hraunfossar - which had been recommended to me by the farmer at Bjarteyjarsandur - I made quick way towards Reykholt. Shopping around for nutritious and packable foods I was aided by the shop attendant, her infant son and an edlerly couple. While I cannot pretend to know the realities of living in the Icelandic hinterland long-term, watching their interactions gave me a hint at what it must be like. I also got to experience the scarcity imposed by remoteness first hand. The locals had to tell me more than once that the product I was asking after was out of stock and fresh supplies would likely only come in spring.

Having scavanged ingredients for another week's worth of trail meals, I topped up my cart with an ice cream cone - which never seem to be in short supply. Oh the joy of finding an opportunity to have unpackable food! I savoured my treat in town, marvelling at how somehow the town hall, the church and the visitor's centre all flowed together into one semi-connected complex incorporating both modern architecture and the 1000+ year old Snorralaug.

Since the sun was shining and the bench I had found seemed especially comfortable, it was decidedly too late to continue towards Harunfossar without imposing a serious sprint on myself by the time I remembered that I still had kilometres to ride. Hence, I turned Twig around 180° and headed towards Deildartunguhver, a hot spring supplying both the spa built next to it and an entire region around it.

This was the first major hot spring I had seen up close and I admit it was quite an experience. Seeing water bubble out of the earth with such force and feeling the cold wind blow sulfur-laden steam into my face felt utterly incoherent. I couldn't even properly capture it in photos - the spectactle was so changeable that I hardly ever had time to compose. I resolved to point my camera at the scene and let the motor drive rip, hoping something would come of it.

<iframe style="width: 100%; aspect-ratio: 2/3; border-radius: var(--spacing-s)" src="https://www.youtube-nocookie.com/embed/M26QYb1HtEw" title="YouTube video player short video of bubbling hot spring" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" frameborder="0" allowfullscreen></iframe>

Impressive as it was, the spectactle quickly became dazzling. I got back on the saddle and pedaled north, then west. When I finally turned onto the road leading to Hotel Varmaland, a song immediately popped into my head.

<iframe style="width: 100%; aspect-ratio: 2; border-radius: var(--spacing-s)" src="https://www.youtube-nocookie.com/embed/09839DpTctU?si=JEK4QDD8teqVuPzM" title="YouTube video player for Hotel California by The Eagles" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

This is exactly what I had always imagined that dark desert highway to look like before sunset. Admittedly, the vegetation likely likely defies the "desert" qualifier and the cool wind in my hair was about 20 degrees Celsius cooler than expected, but the vibes were certainly there!

<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8796.jpg"></bz-picture></full-bleed-section>

With the Eagles' well-tempered guitars in my mind's ear, I rolled up and checked in with the fittingly dapper-looking front desk clerk. I will resist the temptation to weave an annoyingly long list of puns into this tale and just say that I took the opportunity to sit under a mirrored ceiling and waltz through my photos to remember the marvels I had seen so far.

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8802.jpg"></bz-picture>


<picture-background legacyFormat="jpeg" rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/map_varmaland_to_snorrastadhir.jpg" class="text-black">

## Hotel Varmaland to Snorrastaðir
</picture-background>

While crisp linens and a breakfast buffet undoubtedly are lovely, I was itching to get back to wilderness. There was a promising-looking gravel road leading towards the campground at Snorrastaðir and I was excited to eplore it.

<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8803.jpg"></bz-picture></full-bleed-section>

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8817.jpg"></bz-picture>

<full-bleed-section><div class="grid grid-cols-1 md:grid-cols-3 gap-4 w-full px-4 relative">
    <bz-picture rounded loading="lazy" class="col-span-2" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8839.jpg"></bz-picture>
    <div class="prose">

Somehow kilometres never feel as long when the ground crunches under my tires. This path was wonderful in how it ramped up the adventure: tarmac turning to gravel, gravel slowly being broken by smaller streams that can be forded in the saddle, the big river introducing itself with a beautiful view on some fish stairs that are apparently teeming with salmon if you know when to come.
    </div>
</div></full-bleed-section>

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8864.jpg"></bz-picture>

After a few more kilometres of riding alongside this beauty, it was time: I had to get to the other side of this river and there was no way I would stay in the sadle if I attempted to ride across. So, a few deep breaths later, I took off my boots, put on the sneakers, waded in and pushed Twig across. Lord, that cold!

<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8875.jpg"></bz-picture></full-bleed-section>

High on adrenalin, I toweled off my feet, had a snack and a giggle. Fording a _proper_ river had been something of a dream for a while and now I'd done it.

To keep the adventure factor high, I was soon greeted by a large orange sign proclaiming this road impassable 8 km further on. A look at the map told me that I had to turn onto a paved road after just that distance. After some hesitation I concluded that I was willing to take the risk of finding myself faced with an uncrosseable chasm just before I would hit upon the main road, _especially_ since the alternative would have been riding a much longer distance on tarmac. It turned out that "impassable" meant "impassable for cars, no problem for a good bike". Another win on the books.

Buoyed by these adventures off the beaten track and increasingly worried about the fading light, I hurried along the last kilometres of paved road. Part of the way I was accompanied by a herd of Icelandic horses, walking up the hill just beside me. I reached Snorrastaðir just as dark and low-hanging clouds covered the mountaintops and shrouded the sky.

<picture-background legacyFormat="jpeg" rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/map_snorrastadhir_to_cafe.jpg" class="text-black">

## Snorrastaðir to Hjà Góðu Fólki
</picture-background>

<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8898.jpg"></bz-picture></full-bleed-section>

<full-bleed-section><picture-background legacyFormat="jpeg" loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/map_open_field_to_grundarfjoerdhur.jpg" class="text-black">

## Open Field to Grundarfjörður
</picture-background></full-bleed-section>

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8974.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_8977.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9021.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9048.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9056.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9071.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9121.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9231.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9232.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9239.jpg"></bz-picture>
<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9323.jpg"></bz-picture></full-bleed-section>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9402.jpg"></bz-picture>
<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9438.jpg"></bz-picture></full-bleed-section>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9450.jpg"></bz-picture>

<picture-background legacyFormat="jpeg" rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/map_grundarfjoerdhur_to_stykkisholmur.jpg" class="text-black">

## Grundarfjörður to Stykkishólmur
</picture-background>

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9475.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9506.jpg"></bz-picture>
<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9539.jpg"></bz-picture></full-bleed-section>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9546.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9551.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9556.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9561.jpg"></bz-picture>
<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9579.jpg"></bz-picture>
<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9582.jpg"></bz-picture></full-bleed-section>

## Exploring Stykkishólmur

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9592.jpg"></bz-picture>
<full-bleed-section><div class="grid grid-cols-1 md:grid-cols-2 gap-4 w-full px-4 relative">
    <bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9626.jpg"></bz-picture>
    <bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9618.jpg"></bz-picture>
</div></full-bleed-section>

<picture-background legacyFormat="jpeg" rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/map_stykkisholmur_to_tourist_info.jpg" class="text-black">

## Stykkishólmur to Snæfellsnes Information Centre: The Weather Rules Supreme
</picture-background>

<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9665.jpg"></bz-picture></full-bleed-section>

I had been warned about the wind on Iceland, but thus far, I hadn't yet made its acquaintance. Leaving behind the colourful houses of Stykkishólmur, this was about to change. The weather report that morning showed wind speeds up to 90 km/h across the Vatnaleið pass. Heartened by the fact that the gusts would be in my back, I decided to set off nonetheless.

<full-bleed-section><div class="grid grid-cols-1 md:grid-cols-3 gap-4 w-full px-4 relative">
    <bz-picture rounded loading="lazy" class="col-span-2" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9673.jpg"></bz-picture>
    <div class="p4 prose">

I had briefly toyed with the idea of taking the abandoned road past Kerlingarfjall this time around. However, considering that the condition of this road is unknown and I would likely be all by myself, I quickly decided that this would be foolish. Seeing the wind tug at the rocks at the entrance of the pass confirmed this suspicion and as if to drive the point home, I was pushed off my bike for the first time that day while taking this photo.
    </div>
</div></full-bleed-section>

Turning onto the newer Vatnaleið instead, the wind initially pushed me up the hill. Even though the climb is somewhat steep, I could comfortably pedal in the middle of my cassette. In this way I arrived at the Selvallavatn viewpoint I had seen a few days earlier. This time around, I found the scene entirely changed.

<full-bleed-section><div class="grid grid-cols-1 md:grid-cols-2 gap-4 w-full px-4 relative">
    <figure>
        <bz-picture rounded loading="lazy" alt="A waterfall on the Vatnaleidh pass surrounded by lush green" src="/img/bikepacking-iceland/vatnaleidh_falls_clear.jpg"></bz-picture>
        <figcaption>This waterfall was a spectacle. Every change of the light painted the landscape completely anew</figcaption>
    </figure>
    <figure>
        <bz-picture rounded loading="lazy" alt="The same waterfall, snowed in and blown about by wind" src="/img/bikepacking-iceland/vatnaleidh_falls_snowed_in.jpg"></bz-picture>
        <figcaption>Returning here on my way back, the visual theatre was cloaked by snow to clear the stage for the drama played out by the wind. Imagine seeing this scene buffeted by gusts of well above 100 km/h.</figcaption>
    </figure>
</div></full-bleed-section>

Turning around, the lake that used to be the canvas for such a broad spectrum of colour was now creased into waves by the wind.

<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9704.jpg"></bz-picture></full-bleed-section>

Not wanting to waste the fortuitous direction of the wind, I quickly got back onto my bike and continued uphill. Past the highest point, my luck ran out: tailwinds turned into crosswinds and the road was occasionally covered by last night's snow. Leaning hard into the gale, I could advance at a snail's pace and most of the gusts gave enough advance warning to allow me to counter-steer. Some, however, did not, and I tumbled into the soft roadside snow on multiple occasions.

Halfway through, I was ready to give up and hitch a ride with the next passing vehicle. Unfortunately the only candidate who stopped for me didn't have enough space to transport my bike, so I tried my best to channel the Icelandic sheep and pressed on. Thankfully, the wind eventually lost some of its spice, allowing me to make it to the other side, and to Hjà Góðu Fólki. It was lunchtime and I was in dire need of some good soup, from good folk.

<bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9713.jpg"></bz-picture>

This picture is hazy because my camera immediately fogged up when I pulled it out of the bag, but honestly, the look does a good job representing my mental state at the time. Slowly relaxing my tense muscles and reassuring me that I no longer had to lean into the wind to stay upright, this _was_ the best soup I have ever had.

When I'd finished the bowl, I pondered my next steps over a piece of their marvellous cake. The prospect of cycling all the way to Snorrastaðir - the closest fully-fledged campsite - seemed impossible. I could instead cut my ride for the day short and camp at the visitor's information centre again, but that would put me behind schedule for a timely arrival in Borgarnes where I wanted to catch the bus back to Reykjavik.

Still undecided but well fed, I once again said my goodbyes to my hosts at Hjà Góðu Fólki and pointed my bike eastwards. Since the wind was still blowing south, the direction of the road soon made the decision for me: I could not possibly keep on going like this. I pushed Twig the last kilometre to the Snæfellsnes Visitor's Information Centre. Turning north - into the wind - for the tiny stretch from the road to the shelter of the building cemented the thought that pressing on was madness and I was lucky to have made it this far. This was the end of the line for today.

Tapping through route options on my phone, salvation came courtesy of two francophone Canadians with a ready laugh. They kindly loaded Twig into the back of their camper van and offered me a ride all the way to Borgarnes. What would have been two day's worth of riding breezed past the window within the blink of an eye with the help of a warm conversation.

Once arrived in Borgarnes I learned that the bus I had been meaning to take was cancelled due to high winds. Yes, my riding that day had certainly been foolish.

I resolved to treat myself to a night in a hotel instead after this mad, mad day.

## Exploring Borgarnes

Since my bus would only leave in the afternoon, I used the opportunity to look around this town. One thing this short sight-seeing stint impressed on my was just how concentrated the Icelandic population is. Even though you might call Borgarnes a larger town in the close vicinity of the capital, you could easily cross it by bike within five minutes.

<full-bleed-section><div class="grid grid-cols-1 md:grid-cols-2 gap-4 w-full px-4 relative">
    <bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9736.jpg"></bz-picture>
    <bz-picture rounded loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9738.jpg"></bz-picture>
</div></full-bleed-section>

The dramatic-looking sculpture above on the left is dedicated to [Þorgerður Brák, a nursemaid who was said to have met her end at this spot](https://www.lonelyplanet.com/iceland/borgarnes/attractions/brakin/a/poi-sig/1499342/1318076). It seems to me that Icelandic sagas tend to have few happy ends and most of the male heroes are thoroughly deploreable. Unfortunately I did not get an opportunity to ask an Icelander about this stark tendency to violence in the sagas.

With this still on my mind, I looked out across the bay and thought about what had happened the last few days. Perhaps it is not surprising that the stories told here in the first years after the settlement were filled with tragedy, given that nature could easily inflict it. It was all the more beautiful and impressive in this light that the inhabitants of Iceland have managed to weave welcoming warmth into this wild land of theirs.

<full-bleed-section><bz-picture loading="lazy" alt="Alt text" src="/img/bikepacking-iceland/iceland_2023_DSC_9747.jpg"></bz-picture></full-bleed-section>
